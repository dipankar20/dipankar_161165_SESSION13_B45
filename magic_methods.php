<?php
class Person{
    public $name = "Default Name";
    public $address = "Default Address";
    public $phone = "Default Phone Number";
    public static $myStaticProperty;

    public function __construct()
    {
        echo "i'm inside ".__METHOD__."<br/>";
    }
    public function __destruct()
    {
        echo "i'm inside ".__METHOD__."<br/>";
    }
    public function __call($name, $arguments)
    {
        echo "I'm inside ".__METHOD__."<br/>";
        echo "Wrong Method = $name";
        echo "<pre>";
            print_r($arguments);
        echo "</pre>";
    }
    public static function __callStatic($name, $arguments)
    {
        echo "I'm inside ".__METHOD__."<br/>";
        echo "Wrong Static Method = $name";
        echo "<pre>";
        print_r($arguments);
        echo "</pre>";
    }
    public function doSomething(){
        echo "I'm doing something<br/>";
    }

    public static function doFrequently(){
        echo "I'm doing it frequently<br/>";
    }

    public function __set($name,$value){
        echo "I'm inside ".__METHOD__."<br>";
        echo "Wrong Property Name = $name <br/>";
        echo "Value tried to set to that wrong property is = $value<br/>";
    }
    public function __get($name){
        echo "I'm inside ".__METHOD__."<br>";
        echo "Wrong Property Name = $name <br/>";
    }
    public function __isset($name){
        echo "I'm inside ".__METHOD__."<br>";
        echo "Wrong Property Name = $name <br/>";
    }
    public function __unset($name){
        echo "I'm inside ".__METHOD__."<br>";
        echo "Wrong Property Name = $name <br/>";
    }
    public function __sleep()
    {
        return array("name","phone");
    }
    public function __wakeup()
    {
        $this->doSomething();
    }
    public function __toString()
    {
        return "Are you crazy? I'm just an Object! Not a string!<br/>";
    }
    public function __invoke($value)
    {
        echo "I'm inside ".__METHOD__."<br/>";
        echo "value = $value";
    }
}

/*class Dummy{
    public $dummy;
    public function __construct()
    {
        echo "123<br/>";
    }
    public function __destruct()
    {
       echo "789<br/>";
    }
}*/
Person::$myStaticProperty = "Hello Static<br/>";
Person::doFrequently();
Person::doFrequentlyiii("wrong static method parameter");
echo Person::$myStaticProperty;

$person1 = new Person;
echo "Hello World<br/>";
//$person2 = new Dummy();
$person1->do1234Something(12345,"wrong method parameter");
$person1->dob = "set this string";
echo $person1->sfgfjhjfgh;

if(isset($person1->ytjrw)){

}
unset ($person1->ytjrw);
$myVar = serialize($person1);
var_dump($myVar);
echo "<br/>";
$newObj = unserialize($myVar);
var_dump($newObj);
echo "<br/>";
echo $person1."Hello";
$person1(12);
unset($person1);
?>